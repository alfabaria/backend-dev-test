## Backend Developer Test January 2020 Definite

## Prerequisite
Make sure you have the essentials.
- PHP >= 7.2
- Mysql Database
- Postman (To test your endpoints)

## How-to install and build

### 1. Clone the repository

```console
foo@bar:~$ git clone https://gitlab.com/alfabaria/backend-dev-test.git auth-app
foo@bar:~$ cd auth-app
```

### 2. If you don't have composer in your local, install the composer using following command (linux ubuntu)

```console
foo@bar:~$ curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
```
### 3. If you don't have lumen in your local, install the lumen using following command (linux ubuntu)

```console
foo@bar:~$ composer global require "laravel/lumen-installer"
```
### 4. Install the library dependencies using following command

```console
foo@bar:~$ composer install
```

### 5. Copy the .env.example file to .env and change the database connection as per your local configuration

```console
foo@bar:~$ cp .env.example .env
foo@bar:~$ vi .env
```

```console
# customize this line!
DB_DATABASE="<your local database>"
DB_USERNAME="<your local database user>"
DB_PASSWORD="<your local database password>"
```

### 6. Migrate the database using following command

```console
foo@bar:~$ php artisan migrate
```

### 7. Generate your API secret

```console
foo@bar:~$ php artisan jwt:secret
```
### 8. After all, you can start the your lumen local web server.

```console
foo@bar:~$ php -S localhost:8000 -t public
```
Open your browser and navigate to http://127.0.0.1:8000/ . if everything is working, you'll see a welcome page like this "Lumen (6.2.0) (Laravel Components ^6.0)"

### 9. API To create the new user , navigate to 
- URL: http://127.0.0.1:8000/api/register
- HTTP Method: POST
- Parameter (Payload Data):
 
| Param | Type |
| ------ | ------ |
| username | string |
| password | string |


### 10. API to login the user , navigate to 
- URL: http://127.0.0.1:8000/api/login
- HTTP Method: POST
- Parameter (Payload Data):

| Param | Type |
| ------ | ------ |
| username | string |
| password | string |

### 11. API to show all user , navigate to 
- URL: http://127.0.0.1:8000/api/users
- HTTP Method: GET