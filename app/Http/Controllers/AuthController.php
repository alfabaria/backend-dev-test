<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use  App\User;
//import auth facades
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function register(Request $request)
    {
        //validate incoming request 
        /* $this->validate($request, [
            'username' => 'required|string|unique:users',
            'password' => 'required',
        ]); */
		
		$validator = Validator::make($request->all(), [
           'username' => 'required|string|unique:users',
           'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['data' => ['code' => 200 ]], 400);
        }

        try {

            $user = new User;
            $user->username = $request->input('username');
            $plainPassword = $request->input('password');
            $user->password = app('hash')->make($plainPassword);
			$user->plain_password = $plainPassword;

            $user->save();

            //return successful response
            return response()->json(['data' => ['code' => 0]], 200);

        } catch (\Exception $e) {
            //return error message
            return response()->json(['data' => ['code' => 400]], 400);
        }

    }
	
	/**
     * Get a JWT via given credentials.
     *
     * @param  Request  $request
     * @return Response
     */
    public function login(Request $request)
    {
        //validate incoming request 
        $this->validate($request, [
            'username' => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = $request->only(['username', 'password']);

        if (! $token = Auth::attempt($credentials)) {
            return response()->json(['data' => ['code' => 100]], 400);
        }

        // return $this->respondWithToken($token);
		return response()->json(['data' => ['code' => 0,'token' => $token]], 200);
    }


}